package hello;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/*
* Controller de l'interface Web
*/
@Controller
@RequestMapping(path="/demo") // This means URL's start with /demo (after Application path)
public class GreetingController {

    @Autowired 
    private ParticipantRepository partRepo;
    
    /*
    * Methode d'ajout d'un participant par remplissage du formulaire
    * @param : Attributs d'un participant
    */
    @GetMapping(path="/addpart") // Map ONLY GET Requests
    public @ResponseBody String addNewPart (@RequestParam String nom        //Ici juste les noms et prénoms des participants
                    , @RequestParam String prenom,                          // et l'évènement auquel ils participent sont obligatoires
                    @RequestParam(required = false) String email,
                    @RequestParam(required = false) String datenaiss,
                    @RequestParam(required = false) String organisation,
                    @RequestParam(required = false) String obs,
                    @RequestParam Evenement participe) {
            

            Participant n = new Participant(nom, prenom, email, datenaiss, organisation, obs,participe);
            
            /*n.setName(name);
            n.setEmail(email);*/
            
            partRepo.save(n);                                               //Enregistre le contenu du formulaire
            return "Saved";
    }
    
    /*
    * Affiche la liste des participants enregistrés
    */
    @GetMapping(path="/allpart")
    public @ResponseBody Iterable<Participant> getAllPart() {
            
            return partRepo.findAll();
    }
    
    /**
     * Supprime un participant
     * @param : id du participant à supprimer
     */
    @GetMapping(path="/delpart")
    public @ResponseBody String delPart(@RequestParam int numpers){
        Participant n = new Participant(numpers);
        
        partRepo.delete(n);                                             //supprime le participant
        
        return "deleted";
    }
    
    /**
     * Modifie les attributs d'un participant
     * @param : attributs d'un participant
     */
    @GetMapping(path="/updatepart") // Map ONLY GET Requests
    public @ResponseBody String updatePart (@RequestParam int numpers,
                    @RequestParam String nom
                    , @RequestParam String prenom,
                    @RequestParam(required = false) String email,
                    @RequestParam(required = false) String datenaiss,
                    @RequestParam(required = false) String organisation,
                    @RequestParam(required = false) String obs,
                    @RequestParam Evenement participe){
        
        Participant n = new Participant(numpers);
        partRepo.delete(n);                                             //supprime les anciennes valeurs
        
        Participant n2 = new Participant(nom, prenom, email, datenaiss, organisation, obs, participe);
        partRepo.save(n2);                                              //enregistre les nouvelles valeurs 
        
        return "updated";
    }
    
    @Autowired
    private EvenementRepository evRepo;
    
    /**
    * Ajout d'un évènement par remplissage du formulaire
    * @param : attributs d'un évènement
    */
    @GetMapping(path="/addev") // Map ONLY GET Requests
    public @ResponseBody String addNewEv (@RequestParam String intitule
                    , @RequestParam String theme,
                    @RequestParam String dateDeb,
                    @RequestParam String duree,
                    @RequestParam(required = false) int nbPartMax,
                    @RequestParam(required = false) String desc,
                    @RequestParam String organisateur,
                    @RequestParam String typeEven) {
            
            Evenement n = new Evenement(intitule, theme, dateDeb, nbPartMax, nbPartMax, desc, organisateur, typeEven);
            /*n.setName(name);
            n.setEmail(email);*/
            evRepo.save(n);                                             //enregistre les valeurs saisies
            return "Saved";
    }
    
    /*
    * Liste des évènements enregistrés
    */
    @GetMapping(path="/allev")
    public @ResponseBody Iterable<Evenement> getAllEv() {
            // This returns a JSON or XML with the users
            return evRepo.findAll();
    }
    
    /**
     * Supprime un évènement
     * @param : identifiant de l'évènement
     */
    @GetMapping(path="/delev")
    public @ResponseBody String delEV (@RequestParam int numeven) {
        Evenement n = new Evenement(numeven);
        evRepo.delete(n);                                               //supprime l'évènement
        
        return "Deleted";
    }
    
    /**
     * Modifie les attributs d'un évènement
     * @param : attributs d'un évènement
     */
    @GetMapping(path="/updateev")
    public @ResponseBody String updateEV (@RequestParam int numeven,
                            @RequestParam String intitule
                    , @RequestParam String theme,
                    @RequestParam String dateDeb,
                    @RequestParam String duree,
                    @RequestParam(required = false) int nbPartMax,
                    @RequestParam(required = false) String desc,
                    @RequestParam String organisateur,
                    @RequestParam String typeEven) {
                        
        Evenement n = new Evenement(numeven);                           //supprime l'ancien évènement
        evRepo.delete(n);
        
        Evenement n2 = new Evenement(intitule, theme, dateDeb, nbPartMax, nbPartMax, desc, organisateur, typeEven);
        evRepo.save(n2);                                                //enregistre le nouvel évènement
        return "Update";
    }
}

