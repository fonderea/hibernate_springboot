package hello;


import org.springframework.data.repository.CrudRepository;


/**
 * Interface qui implémente le repository du framework Spring
 * 
 */
public interface EvenementRepository extends CrudRepository<Evenement, Integer> {

}
