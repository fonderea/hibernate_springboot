package hello;

import org.springframework.data.repository.CrudRepository;


/**
 * Interface implémentant le repository de Spring
 */
public interface ParticipantRepository extends CrudRepository<Participant, Integer> {

}
