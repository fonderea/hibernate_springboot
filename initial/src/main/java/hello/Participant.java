package hello;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;


/**
 * Classe Participant
 * liée à la table du même nom
 * @author formation
 */
@Entity
@Table(name = "participant")
public class Participant implements Serializable {
    @Id
    @GeneratedValue(generator="increment")                      //identifiant généré automatiquement
    @GenericGenerator(name="increment",strategy="increment")
    @Column(name="num_pers")
    private int numpers;
    
    @Column(name="nom",nullable=false)
    private String nom;
    
    @Column(name="prenom",nullable=false)
    private String prenom;
    
    @Column(name="email", nullable=false)
    private String email;
    
    @Column(name="date_naiss",nullable=false)
    private String datenaiss;
    
    @Column(name="organisation",nullable=false)
    private String organisation;
    
    @Column(name="observations",nullable=false)
    private String obs;
    
    @ManyToOne
    private Evenement participe; //plusieurs participants participent à un évènement
    
                                                                //Constructeurs

    public Participant() {
    }

    public Participant( String nom, String prenom, String email, String datenaiss, String organisation, String obs) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.datenaiss = datenaiss;
        this.organisation = organisation;
        this.obs = obs;
    }

    public Participant(int numpers, String nom, String prenom, String email, String datenaiss, String organisation, String obs) {
        this.numpers = numpers;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.datenaiss = datenaiss;
        this.organisation = organisation;
        this.obs = obs;
    }

    public Participant(String nom, String prenom, String email, String datenaiss, String organisation, String obs, Evenement participe) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.datenaiss = datenaiss;
        this.organisation = organisation;
        this.obs = obs;
        this.participe=participe;
    }   

    public Participant(int numpers) {
        this.numpers = numpers;
    }
    
                                                            //Getters et Setters
    
    public int getNumpers() {
        return numpers;
    }

    public void setNumpers(int numpers) {
        this.numpers = numpers;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDatenaiss() {
        return datenaiss;
    }

    public void setDatenaiss(String datenaiss) {
        this.datenaiss = datenaiss;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public Evenement getParticipe() {
        return participe;
    }

    public void setParticipe(Evenement participe) {
        this.participe = participe;
    }
    
}
