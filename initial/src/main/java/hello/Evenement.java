package hello;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

/**
 * Classe Evenement
 * liée à la table du même nom
 * @author formation
 */
@Entity
@Table(name="evenement")
public class Evenement implements Serializable {
    @Id
    @GeneratedValue(generator="increment")                      //identifiant généré automatiquement
    @GenericGenerator(name="increment",strategy="increment")
    @Column(name="num_even")
    private int numeven;
    
    @Column(name="intitule",nullable=false)
    private String intitule;
    
    @Column(name="theme",nullable=false)
    private String theme;
    
    @Column(name="date_debut",nullable=false)
    private String dateDeb;
    
    @Column(name="duree",nullable=false)
    private int duree;
    
    @Column(name="nb_part_max",nullable=false)
    private int nbPartMax;
    
    @Column(name="description",nullable=false)
    private String desc;
    
    @Column(name="organisateur",nullable=false)
    private String organisateur;
    
    @Column(name="type_even",nullable=false)
    private String typeEven;
    
    @OneToMany(mappedBy="participe")
    private List<Participant> parts = new ArrayList<>();        //un évènement a plusieurs participants
    
                                                                //Constructeurs

    public Evenement() {
    }


    public Evenement(String intitule, String theme, String dateDeb, int duree, int nbPartMax, String desc, String organisateur, String typeEven, List<Participant> parts) {
        this.intitule = intitule;
        this.theme = theme;
        this.dateDeb = dateDeb;
        this.duree = duree;
        this.nbPartMax = nbPartMax;
        this.desc = desc;
        this.organisateur = organisateur;
        this.typeEven = typeEven;
        this.parts = parts;
    }

    public Evenement(String intitule, String theme, String dateDeb, int duree, int nbPartMax, String desc, String organisateur, String typeEven) {
        this.intitule = intitule;
        this.theme = theme;
        this.dateDeb = dateDeb;
        this.duree = duree;
        this.nbPartMax = nbPartMax;
        this.desc = desc;
        this.organisateur = organisateur;
        this.typeEven = typeEven;
    }

    public Evenement(int numeven) {
        this.numeven = numeven;
    }

                                            //Getters et Setters

    public int getNumeven() {
        return numeven;
    }

    public String getIntitule() {
        return intitule;
    }

    public String getTheme() {
        return theme;
    }

    public String getDateDeb() {
        return dateDeb;
    }

    public int getDuree() {
        return duree;
    }

    public int getNbPartMax() {
        return nbPartMax;
    }

    public String getDesc() {
        return desc;
    }

    public String getOrganisateur() {
        return organisateur;
    }

    public String getTypeEven() {
        return typeEven;
    }

    public List<Participant> getParts() {
        return parts;
    }

    public void setNumeven(int numeven) {
        this.numeven = numeven;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setDateDeb(String dateDeb) {
        this.dateDeb = dateDeb;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public void setNbPartMax(int nbPartMax) {
        this.nbPartMax = nbPartMax;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setOrganisateur(String organisateur) {
        this.organisateur = organisateur;
    }

    public void setTypeEven(String typeEven) {
        this.typeEven = typeEven;
    }

    public void setParts(List<Participant> parts) {
        this.parts = parts;
    }
    
    
}
