# TP Hibernate et SpringBoot

## Connexion à la Base de Données
La connexion à la base de données se fait grâce à Hibernate, un Framework open source qui permet la persistance d'objets en bases de données relationnelles. Ce Framework demande au préalable la création de la base de données dans un système de gestion de bases de données (PgAdmin pour PostgresSQL par exemple). Côté java, le paramétrage de la connexion à la base de données se fait grâce au fichier application.properties : 


`*spring.jpa.hibernate.ddl-auto=update*`

`*spring.datasource.url=jdbc:postgresql://localhost:5432/hibernate*`

`*spring.datasource.username=formation*`

`*spring.datasource.password=formation*`

Le mot clé "update" permet de paramétrer de demander à ce que à chaque fin d'utilisation de l'application, la base de données ne soit pas vidée, mais que l'on conserve les données d'un lancement à un autre.

## Création des tables de données
La création des tables de données se fait par le biais de classes correspondant respectivement aux Participants et aux Evènements. Toujours avec Hibernate, on dispose d'annotations pour paramétrer la mise en place de tables. On met donc en place deux tables, évènement et participant avec leurs attributs respectifs qui auront chacun leur colonne correspondante. On crée aussi par le même temps les getters/setter qui nous permettront d'accéder aux valeurs des entités de chaque table.
Aussi, Hibernate nous permet de mettre en place une relation entre les deux tables puisqu'un participant participe à un évènement ("ManyToOne"), et un évènement comprend plusieurs participants ("OneToMany").

## Ajout, suppression et modification
Grâce à SpringBoot on peut alors faire interagir un utilisateur avec la base de données précédemment créée. En effet par l'intermédiaire de requêtes paramétrées par les annotations Spring, on peut disposer des fonctionnalités du repository (*EvenementRepository*, *ParticipantRepository*) inclues dans Spring et les adapter aux deux classes Evènement et Participant : `.findAll()`, `.save()`, `.delete()`. Toutes les requêtes sont regroupées dans un Controller (*GreetingsController*).

## Intéraction Web/Base de données
L'utilisateur passe par une page html contenant des formulaires qui lancent par la suite l'ouverture des urls correspondants aux différentes requêtes paramétrées dans le controller.